package it.unica.salvatorespanu.wearsensorsrecorder;


import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;

import java.text.SimpleDateFormat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.Instance;
import weka.core.SerializationHelper;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements
        DataClient.OnDataChangedListener,
        MessageClient.OnMessageReceivedListener,
        CapabilityClient.OnCapabilityChangedListener {

    private static final String TAG = "MainActivity";
    private static final String COUNT_KEY = "it.unica.salvatorespanu.count";
    private BufferedWriter file;
    private BufferedWriter file2;
    private TextView filenameDisplay;
    private TextView logDisplay;
    private TextView recTime;
    Button driverButtonU, driverButtonE, driverButtonM,
            passengerButtonU, passengerButtonE, passengerButtonM, stop;
    private String routeType;
    private String activityType;
    private int n_records = 0;
    private long i_timestamp;
    public int k=0;
    public int N = 10;
    public long vectortimestamp[] =  new long[N];
    public String vectorsensortype[] =  new String[N];
    public int vectoraccuracy[] =  new int[N];
    public float vectorx[] = new float[N];
    public float vectory[] = new float[N];
    public float vectorz[] = new float[N];
    public String vectoractivity[] = new String[N];
    public float total_acc[] = new float[N];
    public float tiltx[] = new float[N];
    public float tilty[] = new float[N];
    public float tiltz[] = new float[N];
    public int z = 0;
    public int n_driver = 0;
    public int n_passenger = 0;
    public int cont = 0;

    public double prediction;

    Classifier cls;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id._start_drive_u).setOnClickListener(clickListener);
        findViewById(R.id._stop).setOnClickListener(clickListener);
        findViewById(R.id._start_activity).setOnClickListener(clickListener);


        driverButtonU = (Button) findViewById(R.id._start_drive_u);
        stop = (Button) findViewById(R.id._stop);
        filenameDisplay = (TextView) findViewById(R.id.filename);
        logDisplay = (TextView) findViewById(R.id.log);
        recTime = (TextView) findViewById(R.id.uptime);
    }


    @Override
    protected void onResume(){
        super.onResume();
        Wearable.getDataClient(this).addListener(this);
        Wearable.getMessageClient(this).addListener(this);
        Wearable.getCapabilityClient(this).addListener(
                this, Uri.parse("wear://"),
                CapabilityClient.FILTER_REACHABLE);
        Log.i("info", "Listener attivo");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Wearable.getDataClient(this).removeListener(this);
        Wearable.getMessageClient(this).removeListener(this);
        Wearable.getCapabilityClient(this).removeListener(this);
        Log.i("info", "Listener rimosso");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents){
        //Log.i("info", "Dati cambiati");
        for(DataEvent event : dataEvents){
            if (event.getType() == DataEvent.TYPE_CHANGED){
                //DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/count") == 0){
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    updateCount(dataMap.getInt(COUNT_KEY));
                }

                if (item.getUri().getPath().compareTo("/activityStarted") == 0){
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    toast("Activity started on smartwatch");
                }

                if (item.getUri().getPath().compareTo("/sensors")==0){
                    unpackSensorData(DataMapItem.fromDataItem(item).getDataMap());
                }
            }else if (event.getType() == DataEvent.TYPE_DELETED){
                // DataItem deleted
            }
        }
    }

    private void unpackSensorData(DataMap dataMap) {
        String sensorType = dataMap.getString("sensorType");
        int accuracy = dataMap.getInt("accuracy");
        long timestamp = dataMap.getLong("timestamp");
        float[] values = dataMap.getFloatArray("values");
        //write(timestamp, sensorType, accuracy, values);
        featureVector(timestamp, sensorType, accuracy, values);
        //Log.i(TAG, "Received sensor data " + sensorType + " = " + Arrays.toString(values));
    }

    private void updateCount(int c){
        Log.i("info", "contatore: " + c);
        Toast toast1 = Toast.makeText(
                getBaseContext(),
                "Counter ricevuto: " + c,
                Toast.LENGTH_SHORT);
        toast1.show();
    }

    /**
     * Sends an RPC to start a fullscreen Activity on the wearable.

    public void onStartWearableActivityClick(View view){
        new StartWearableActivityTask("start").execute();
    }

    */



    private class StartWearableActivityTask extends AsyncTask<Void, Void, Void> {
        private String type;
        public StartWearableActivityTask(String type){
            this.type = type;
        }
        @Override
        protected Void doInBackground(Void... args){
            Collection<String> nodes = getNodes();
            for (String node : nodes){
                sendStartActivityMessage(node, type);
            }
            return null;
        }
    }

    @WorkerThread
    private void sendStartActivityMessage(String node, String type){
        Task<Integer> sendMessageTask = null;
        switch (type){
            case "start":
                sendMessageTask = Wearable.getMessageClient(this)
                        .sendMessage(node, "/start-activity", new byte[0]);
                break;

            case "drive":
                sendMessageTask = Wearable.getMessageClient(this)
                        .sendMessage(node, "/start-drive", new byte[0]);
                break;

            case "passenger":
                sendMessageTask = Wearable.getMessageClient(this)
                        .sendMessage(node, "/start-passenger", new byte[0]);
                break;

            case "stop":
                sendMessageTask = Wearable.getMessageClient(this)
                        .sendMessage(node, "/stop", new byte[0]);
                break;
        }

        try{
            Integer result = Tasks.await(sendMessageTask);
            Log.i(TAG, "Message sent: " + result);
        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);
        } catch (InterruptedException exception){
            Log.e(TAG, "Interrupt occurred: " + exception);
        }
    }

    @WorkerThread
    private Collection<String> getNodes(){
        HashSet<String> results = new HashSet<>();
        Task<List<Node>> nodeListTask = Wearable.getNodeClient(getApplicationContext()).getConnectedNodes();
        try{
            List<Node> nodes = Tasks.await(nodeListTask);
            for(Node node : nodes){
                results.add(node.getId());
            }
        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);

        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }
        return results;
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id._start_activity:
                    new StartWearableActivityTask("start").execute();
                    break;
                case R.id._start_drive_u:
                    activityType = "driver";
                    routeType = "urban";
                    startRecording("_driver_urban");
                    new StartWearableActivityTask("drive").execute();
                    driverButtonU.setBackgroundColor(Color.RED);
                    //toast("Driver - urban route");
                    break;
                case R.id._stop:
                    activityType = "";
                    routeType = "";
                    stopRecording();
                    new StartWearableActivityTask("stop").execute();
                    driverButtonU.setBackgroundColor(Color.GRAY);
                    //toast("Stop recording");
                    break;
            }
        }

    };



    private void toast(String message){
        Toast toast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void startRecording(String type) {
        if (isExternalStorageWritable()) {
            n_records = 0;
            i_timestamp = 0;

            // creazione del file
            File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            Log.i("Info", "Directory: " + directory.toString());
            //String name = System.currentTimeMillis() + type + ".csv";
            String name = getDateTime() + type + ".csv";
            File filename = new File(directory, name);
            try {
                file = new BufferedWriter(new FileWriter(filename));
                Log.i("Info", "File creato");
            } catch (IOException e) {
                Log.i("Info", "File NON creato");
                e.printStackTrace();
            }
            // visualizzazione del nome del file
            filenameDisplay.setText("File " + name + " recording");
        }
        else {
            Log.i(TAG, "Memoria non accessibile");
        }

        if (isExternalStorageWritable()) {
            n_records = 0;
            i_timestamp = 0;

            // creazione del file
            File directory2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            Log.i("Info", "Directory: " + directory2.toString());
            //String name = System.currentTimeMillis() + type + ".csv";
            String name2 = "features" + ".csv";
            File filename = new File(directory2, name2);
            try {
                file2 = new BufferedWriter(new FileWriter(filename));
                Log.i("Info", "File creato");
            } catch (IOException e) {
                Log.i("Info", "File NON creato");
                e.printStackTrace();
            }
            // visualizzazione del nome del file
            filenameDisplay.setText("File " + name2 + " recording");

        }
        else {
            Log.i(TAG, "Memoria non accessibile");
        }
    }

    private void stopRecording() {
/*
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            file2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
    }

    private void write(Long timestamp, String typeSensor, int accuracy, float[] values) {

        n_records++;

        long timeInMillis = (new Date()).getTime()
                + (timestamp - System.nanoTime()) / 1000000L;

        if(i_timestamp == 0){
            i_timestamp = timeInMillis-1000;
        }

        float recordRate = n_records/((timeInMillis-i_timestamp)/1000);

        String[] array = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            array[i] = Float.toString(values[i]);
        }

        recTime.setText(Float.toString(((timeInMillis-i_timestamp)/1000)));

        write(Long.toString(timeInMillis), typeSensor, Integer.toString(accuracy), array,
                getDateTime(), recordRate);


    }

    private void write(String timestamp, String typeSensor, String accuracy, String[] values,
                       String date, float recordRate) {
        if (file == null) {
            return;
        }

        String line = "";
        if (values != null) {
            for (String value : values) {
                line += "," + value;
            }
        }
        line = timestamp + "," + typeSensor + "," + accuracy + line + ","
                + activityType + "," + routeType + "," + date +"\n";

        try {
            file.write(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

        logDisplay.setText(Integer.toString(n_records)+"/"+Float.toString(recordRate));


    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MM-yyyy_HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    @Override
    public void onMessageReceived(final MessageEvent messageEvent) {

    }

    @Override
    public void onCapabilityChanged(@NonNull CapabilityInfo capabilityInfo) {

    }

    /* Verifica se la memoria esterna dello smartphone è accessibile in lettura e scrittura */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public void featureVector(Long timestamp, String typeSensor, int accuracy, float[] values){

        Attribute duration = new Attribute("duration");
        Attribute max_x = new Attribute("max_x");
        Attribute max_y = new Attribute("max_y");
        Attribute max_z = new Attribute("max_z");
        Attribute max_tot_acc = new Attribute("max_tot_acc");
        Attribute max_tilt_x = new Attribute("max_tilt_x");
        Attribute max_tilt_y= new Attribute("max_tilt_y");
        Attribute max_tilt_z = new Attribute("max_tilt_z");
        Attribute min_x = new Attribute("min_x");
        Attribute min_y = new Attribute("min_y");
        Attribute min_z = new Attribute("min_z");
        Attribute min_tot_acc = new Attribute("min_tot_acc");
        Attribute min_tilt_x = new Attribute("min_tilt_x");
        Attribute min_tilt_y= new Attribute("min_tilt_y");
        Attribute min_tilt_z = new Attribute("min_tilt_z");
        Attribute avg_x = new Attribute("avg_x");
        Attribute avg_y = new Attribute("avg_y");
        Attribute avg_z = new Attribute("avg_z");
        Attribute avg_tot_acc = new Attribute("avg_tot_acc");
        Attribute avg_tilt_x = new Attribute("avg_tilt_x");
        Attribute avg_tilt_y= new Attribute("avg_tilt_y");
        Attribute avg_tilt_z = new Attribute("avg_tilt_z");
        Attribute harmean_tot_acc = new Attribute("harmean_tot_acc");
        Attribute med_x = new Attribute("med_x");
        Attribute med_y = new Attribute("med_y");
        Attribute med_z = new Attribute("med_z");
        Attribute med_tot_acc = new Attribute("med_tot_acc");
        Attribute med_tilt_x = new Attribute("med_tilt_x");
        Attribute med_tilt_y= new Attribute("med_tilt_y");
        Attribute med_tilt_z = new Attribute("med_tilt_z");
        Attribute max_abs_x = new Attribute("max_abs_x");
        Attribute max_abs_y = new Attribute("max_abs_y");
        Attribute max_abs_z = new Attribute("max_abs_z");
        Attribute max_abs_tot_acc = new Attribute("max_abs_tot_acc");
        Attribute max_abs_tilt_x = new Attribute("max_abs_tilt_x");
        Attribute max_abs_tilt_y= new Attribute("max_abs_tilt_y");
        Attribute max_abs_tilt_z = new Attribute("max_abs_tilt_z");

        if(k == 10){

            List<ArrayList> list_features = new ArrayList<>();
            ArrayList features = new ArrayList();
            features.add(vectortimestamp[k-1] - vectortimestamp[0]);
            features.add(maxV(vectorx));
            features.add(maxV(vectory));
            features.add(maxV(vectorz));
            features.add(maxV(total_acc));
            features.add(maxV(tiltx));
            features.add(maxV(tilty));
            features.add(maxV(tiltz));

            features.add(minV(vectorx));
            features.add(minV(vectory));
            features.add(minV(vectorz));
            features.add(minV(total_acc));
            features.add(minV(tiltx));
            features.add(minV(tilty));
            features.add(minV(tiltz));

            features.add(media(vectorx));
            features.add(media(vectory));
            features.add(media(vectorz));
            features.add(media(total_acc));
            features.add(media(tiltx));
            features.add(media(tilty));
            features.add(media(tiltz));

            features.add(harmonicMean(total_acc));

            features.add(median(vectorx));
            features.add(median(vectory));
            features.add(median(vectorz));
            features.add(median(total_acc));
            features.add(median(tiltx));
            features.add(median(tilty));
            features.add(median(tiltz));

            features.add(Math.max(maxV(vectorx), Math.abs(minV(vectorx))));
            features.add(Math.max(maxV(vectory), Math.abs(minV(vectorx))));
            features.add(Math.max(maxV(vectorz), Math.abs(minV(vectorx))));
            features.add(Math.max(maxV(total_acc), Math.abs(minV(total_acc))));
            features.add(Math.max(maxV(tiltx), Math.abs(minV(tiltx))));
            features.add(Math.max(maxV(tilty), Math.abs(minV(tilty))));
            features.add(Math.max(maxV(tiltz), Math.abs(minV(tiltz))));



  /*          try {
                file2.write(String.valueOf(features)+"\n");

            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "Received sensor data " + features);*/
            ArrayList<Attribute> atts = new ArrayList<Attribute>();
            ArrayList<String> classVal = new ArrayList<String>();

            classVal.add("driver");
            classVal.add("passenger");


            atts.add(duration);
            atts.add(max_x);
            atts.add(max_y);
            atts.add(max_z);
            atts.add(max_tot_acc);
            atts.add(max_tilt_x);
            atts.add(max_tilt_y);
            atts.add(max_tilt_z);
            atts.add(min_x);
            atts.add(min_y);
            atts.add(min_z);
            atts.add(min_tot_acc);
            atts.add(min_tilt_x);
            atts.add(min_tilt_y);
            atts.add(min_tilt_z);
            atts.add(avg_x);
            atts.add(avg_y);
            atts.add(avg_z);
            atts.add(avg_tot_acc);
            atts.add(avg_tilt_x);
            atts.add(avg_tilt_y);
            atts.add(avg_tilt_z);
            atts.add(harmean_tot_acc);
            atts.add(med_x);
            atts.add(med_y);
            atts.add(med_z);
            atts.add(med_tot_acc);
            atts.add(med_tilt_x);
            atts.add(med_tilt_y);
            atts.add(med_tilt_z);
            atts.add(max_abs_x);
            atts.add(max_abs_y);
            atts.add(max_abs_z);
            atts.add(max_abs_tot_acc);
            atts.add(max_abs_tilt_x);
            atts.add(max_abs_tilt_y);
            atts.add(max_abs_tilt_z);
            atts.add(new Attribute("@@class@@", classVal));

            Instances dataRaw = new Instances("TestInstances", atts, 0);
            double[] instanceValue = new double[dataRaw.numAttributes()];


            instanceValue[0] = vectortimestamp[k-1] - vectortimestamp[0];
            instanceValue[1] = maxV(vectorx);
            instanceValue[2] = maxV(vectory);
            instanceValue[3] = maxV(vectorz);
            instanceValue[4] = maxV(total_acc);
            instanceValue[5] = maxV(tiltx);
            instanceValue[6] = maxV(tilty);
            instanceValue[7] = maxV(tilty);
            instanceValue[8] = minV(vectorx);
            instanceValue[9] = minV(vectory);
            instanceValue[10] = minV(vectorz);
            instanceValue[11] = minV(total_acc);
            instanceValue[12] = minV(tiltx);
            instanceValue[13] = minV(tilty);
            instanceValue[14] = minV(tiltz);
            instanceValue[15] = media(vectorx);
            instanceValue[16] = media(vectory);
            instanceValue[17] = media(vectorz);
            instanceValue[18] = media(total_acc);
            instanceValue[19] = media(tiltx);
            instanceValue[20] = media(tilty);
            instanceValue[21] = media(tiltz);
            instanceValue[22] = harmonicMean(total_acc);
            instanceValue[23] = median(vectorx);
            instanceValue[24] = median(vectory);
            instanceValue[25] = median(vectorz);
            instanceValue[26] = median(total_acc);
            instanceValue[27] = median(tiltx);
            instanceValue[28] = median(tilty);
            instanceValue[29] = median(tiltz);
            instanceValue[30] = Math.max(maxV(vectorx), Math.abs(minV(vectorx)));
            instanceValue[31] = Math.max(maxV(vectory), Math.abs(minV(vectory)));
            instanceValue[32] = Math.max(maxV(vectorz), Math.abs(minV(vectorz)));
            instanceValue[33] = Math.max(maxV(total_acc), Math.abs(minV(total_acc)));
            instanceValue[34] = Math.max(maxV(tiltx), Math.abs(minV(tiltx)));
            instanceValue[35] = Math.max(maxV(tilty), Math.abs(minV(tilty)));
            instanceValue[36] = Math.max(maxV(tiltz), Math.abs(minV(tiltz)));
            instanceValue[37] = 0;

            dataRaw.add(new DenseInstance(1.0, instanceValue));
            dataRaw.setClassIndex(37);

            Instances testData = dataRaw;
            double pred;

            cont +=1;

            try {
                cls = (Classifier) SerializationHelper.read(getAssets().open("model.model"));
                pred = cls.classifyInstance(testData.instance(0));
                Log.i(TAG, "Received sensor data " + pred);

                if(pred == 0)
                    n_driver +=1;
                else
                    n_passenger +=1;

                if(cont == 10){
                    if(n_driver > n_passenger){
                        //String className = classVal.get(new Double(pred).intValue());
                        filenameDisplay.setText("Driver");
                        cont = 0;
                        n_passenger = 0;
                        n_driver = 0;
                    }else{
                        filenameDisplay.setText("Passenger");
                        cont = 0;
                        n_passenger = 0;
                        n_driver = 0;
                    }
                }

            } catch (Exception e) {
                Log.i(TAG, "Errore");
            }





            k = 0;
            //Predict(features);

            list_features.add(features);
            features.clear();


            //Log.i(TAG, "Received sensor data " + Arrays.toString(test));


        }else{
            vectortimestamp[k] = timestamp;
            vectorsensortype[k] = typeSensor;
            vectoraccuracy[k] = accuracy;
            vectorx[k] = (values[0]/1000);
            vectory[k] = (values[1]/1000);
            vectorz[k] = (values[2]/1000);
            vectoractivity[k] = activityType;
            total_acc[k] = (float) Math.sqrt(vectorx[k]*vectorx[k] + vectory[k]*vectory[k] + vectorz[k]*vectorz[k]);
            tiltx[k] = (float) Math.asin(vectorx[k] / total_acc[k]);
            tilty[k] = (float) Math.asin(vectory[k] / total_acc[k]);
            tiltz[k] = (float) Math.asin(vectorz[k] / total_acc[k]);

            k++;
        }
    }

    public float maxV(float[] vector) {
        float max = 0;
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] > max) {
                max = vector[i];
            }
        }
        return max;
    }

    public float minV(float[] vector) {
        float min = vector[0];
        for (int i = 1; i < vector.length; i++) {
            if (vector[i] < min) {
                min = vector[i];
            }
        }
        return min;
    }

    public float media(float[] vector){
        float somma = 0;
        float media = 0;
        int i;
        for(i=0; i<=vector.length-1; i++) {
            somma=somma+vector[i];
        }

        media=somma/vector.length;
        
        return media;

    }

    public float harmonicMean(float[] data){
        float sum = 0;

        for (int i = 0; i < data.length; i++) {
            sum += 1 / data[i];
        }
        return data.length / sum;
    }

    public float median (float[] vector) {
            Arrays.sort(vector);
            // Calculate median (middle number)
            double median = 0;
            double pos1 = Math.floor((vector.length - 1.0) / 2.0);
            double pos2 = Math.ceil((vector.length - 1.0) / 2.0);
            if (pos1 == pos2 ) {
                median = vector[(int)pos1];
            } else {
                median = (vector[(int)pos1] + vector[(int)pos2]) / 2.0 ;
            }
            return (float) median;
        }



}



