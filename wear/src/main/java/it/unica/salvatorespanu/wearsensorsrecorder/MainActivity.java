package it.unica.salvatorespanu.wearsensorsrecorder;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends WearableActivity implements
        DataClient.OnDataChangedListener,
        MessageClient.OnMessageReceivedListener,
        CapabilityClient.OnCapabilityChangedListener,
        SensorEventListener {

    private static final String TAG = "MainActivity";
    private Button mButton;
    //private static final String COUNT_KEY = "it.unica.salvatorespanu.count";
    private int count = 0;
    private boolean isRecording = false;

    private final static int SENS_ACCELEROMETER = Sensor.TYPE_ACCELEROMETER;
    private final static int SENS_GYROSCOPE = Sensor.TYPE_GYROSCOPE;
    private final static int SENS_GRAVITY = Sensor.TYPE_GRAVITY;

    private long delayForSendingAccelerometer;
    private long delayForSendingGyroscope;
    private long delayForSendingGravitometer;

    SensorManager mSensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //findViewById(R.id.button).setOnClickListener(clickListener);

        //mButton = (Button) findViewById(R.id.button);
        // Enables Always-on
        setAmbientEnabled();
    }


    @Override
    protected void onResume(){
        super.onResume();
        // avvio dei listener per dati e messaggi
        Wearable.getDataClient(this).addListener(this);
        Wearable.getMessageClient(this).addListener(this);
        Wearable.getCapabilityClient(this).addListener(
                this, Uri.parse("wear://"), CapabilityClient.FILTER_REACHABLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // rimozione listener
        Wearable.getDataClient(this).removeListener(this);
        Wearable.getMessageClient(this).removeListener(this);
        Wearable.getCapabilityClient(this).removeListener(this);

    }

    /*
    private void increaseCounter(){
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/count");
        putDataMapReq.getDataMap().putInt(COUNT_KEY, count++);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        putDataReq.setUrgent();
        Task<DataItem> dataItemTask = Wearable.getDataClient(this).putDataItem(putDataReq);
        dataItemTask.addOnSuccessListener(new OnSuccessListener<DataItem>() {
            @Override
            public void onSuccess(DataItem dataItem) {
                Log.i(TAG,"Sending count was successful: " + dataItem);
            }
        });
    }


    private View.OnClickListener clickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v){
            increaseCounter();
            Log.i("counter", "contatore: " + count);
            Toast toast0 = Toast.makeText(
                    getBaseContext(),
                    "Counter send",
                    Toast.LENGTH_SHORT);
            toast0.show();
        }
    };

    */

    @Override
    public void onDataChanged(@NonNull DataEventBuffer dataEventBuffer) {

    }

    @Override
    public void onMessageReceived(@NonNull MessageEvent messageEvent) {
        if (messageEvent.getPath().equals("/start-drive")){
            isRecording = true;
            startMeasurement();
            // inserire flag per invio dati sensori
            Log.i(TAG,"messaggio ricevuto: drive");
            Toast toast1 = Toast.makeText(
                    getBaseContext(),
                    "Driver attivo",
                    Toast.LENGTH_SHORT);
            toast1.show();
        }

        /* ho inserito anche l'opzione passenger nonostante non faccia nulla in più di drive per
        eventualmente distinguere l'attività con un messaggio a display        */

        if (messageEvent.getPath().equals("/start-passenger")){
            isRecording = true;
            startMeasurement();
            Log.i(TAG,"messaggio ricevuto: passenger");
            Toast toast2 = Toast.makeText(
                    getBaseContext(),
                    "Passenger attivo",
                    Toast.LENGTH_SHORT);
            toast2.show();
        }

        if (messageEvent.getPath().equals("/stop")){
            stopMeasurement();
            isRecording = false;
            Log.i(TAG,"messaggio ricevuto: stop");
            Toast toast3 = Toast.makeText(
                    getBaseContext(),
                    "Stop registrazione",
                    Toast.LENGTH_SHORT);
            toast3.show();
        }
    }

    @Override
    public void onCapabilityChanged(@NonNull CapabilityInfo capabilityInfo) {

    }

    protected void startMeasurement(){
        mSensorManager = ((SensorManager) getSystemService(SENSOR_SERVICE));

        Sensor accelerometerSensor = mSensorManager.getDefaultSensor(SENS_ACCELEROMETER);
        //Sensor gravitySensor = mSensorManager.getDefaultSensor(SENS_GRAVITY);
        //Sensor gyroscopeSensor = mSensorManager.getDefaultSensor(SENS_GYROSCOPE);

        if (mSensorManager != null){

            if (accelerometerSensor != null){
                mSensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
            } else {
                Log.w(TAG, "No accelerometer found");
            }
            /*
            if (gravitySensor != null){
                mSensorManager.registerListener(this, gravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
            } else {
                Log.w(TAG, "No gravity sensor found");
            }

            if (gyroscopeSensor != null){
                mSensorManager.registerListener(this, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);
            } else {
                Log.w(TAG, "No gyroscope found");
            }
            */
        }
    }

    private void stopMeasurement(){
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
            Log.i(TAG,"Listener spento");
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final int delay = 0; // in millisecondi
        // con 10 su 30sec ne perde circe 8 di registrazione file da 392 records di 32kb 5,75/sec registraazioni per ogni sensore
        // con 100 non perde dati. file da 608 records di 48kb 6,75/sec registrazioni
        // con 50 non perde dati. file da 650 records di 52kb 7,17/sec registrazioni
        if(isRecording) {
            if(event.sensor.getName().equals("Accelerometer Sensor")
                    && Math.abs(delayForSendingAccelerometer-System.currentTimeMillis()) > delay) {
                sendSensorData(event.sensor.getName(), event.accuracy, event.timestamp, event.values);
                delayForSendingAccelerometer = System.currentTimeMillis();
            }
            if(event.sensor.getName().equals("Gyroscope Sensor")
                    && Math.abs(delayForSendingGyroscope-System.currentTimeMillis()) > delay) {
                sendSensorData(event.sensor.getName(), event.accuracy, event.timestamp, event.values);
                delayForSendingGyroscope = System.currentTimeMillis();
            }
            if(event.sensor.getName().equals("Gravity Sensor")
                    && Math.abs(delayForSendingGravitometer-System.currentTimeMillis()) > delay) {
                sendSensorData(event.sensor.getName(), event.accuracy, event.timestamp, event.values);
                delayForSendingGravitometer = System.currentTimeMillis();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void sendSensorData(
            final String sensorType, final int accuracy, final long timestamp, final float[] values){

            PutDataMapRequest dataMap = PutDataMapRequest.create("/sensors");

            dataMap.getDataMap().putString("sensorType", sensorType);
            dataMap.getDataMap().putInt("accuracy", accuracy);
            dataMap.getDataMap().putLong("timestamp", timestamp);
            dataMap.getDataMap().putFloatArray("values", values);

            PutDataRequest putDataRequest = dataMap.asPutDataRequest();
            putDataRequest.setUrgent();
            Task<DataItem> dataItemTask = Wearable.getDataClient(this).putDataItem(putDataRequest);
            dataItemTask.addOnSuccessListener(new OnSuccessListener<DataItem>() {
                @Override
                public void onSuccess(DataItem dataItem) {
                    Log.i(TAG, "Sending data sensors was successful: " + dataItem);
                }
            });
    }
}
