package it.unica.salvatorespanu.wearsensorsrecorder;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.MessageClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

public class DataLayerListenerService extends WearableListenerService implements
        DataClient.OnDataChangedListener,
        MessageClient.OnMessageReceivedListener,
        CapabilityClient.OnCapabilityChangedListener  {

    private static final String TAG = "ListenerService";


    public void onCreate(){
        super.onCreate();
        Wearable.getMessageClient(this).addListener(this);
        Log.i("info", "Listener attivo");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent){

        if (messageEvent.getPath().equals("/start-activity")){
            Log.i(TAG,"messaggio ricevuto: start activity");
            Intent startIntent = new Intent(this, MainActivity.class);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(startIntent);
        }
    }

/*
    private static final String TAG = "DataLayerService";
    GoogleApiClient mGoogleApiClient;


    @Override
    public void onCreate(){
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
        Log.i(TAG,"GoogleApiClient connected");
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.i(TAG, "onDataChanged");
        if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
            ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()){
                Log.e(TAG, "DataLayerListenerService failed to connect to GoogleApiClient, "
                        + "error code: " + connectionResult.getErrorCode());
                return;
            }
        }

        for(DataEvent event : dataEvents){
            Uri uri = event.getDataItem().getUri();
            String path = uri.getPath();
            if ("/count".equals(path)){
                String nodeId = uri.getHost();
                byte[] payload = uri.toString().getBytes();
                // send the RPC
                Wearable.MessageApi.sendMessage(mGoogleApiClient, nodeId, "/data-item-received", payload );
            }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent){

        if (messageEvent.getPath().equals("/start-activity")){
            Log.i(TAG,"messaggio ricevuto: start activity");
            Intent startIntent = new Intent(this, MainActivity.class);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(startIntent);
        }
    }
    */
}
